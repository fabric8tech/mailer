<?php

if (!function_exists('mailer')) {
    /**
     * @param \Fabric8\Mailer\MailerContract|null $transport
     * @return \Fabric8\Mailer\Mailer
     */
    function mailer(\Fabric8\Mailer\MailerContract $transport = null)
    {
        static $mailer;

        if (!isset($mailer)) {
            if (!isset($transport)) {
                throw new InvalidArgumentException('Parameter transport is required');
            }

            $mailer = new \Fabric8\Mailer\Mailer($transport);
        }

        return $mailer;
    }
}

if (!function_exists('mailgun')) {
    /**
     * @param string $api
     * @param string $domain
     * @return \Fabric8\Mailer\Mailgun
     */
    function mailgun($api = null, $domain = null)
    {
        static $mailgun;

        if (!isset($mailgun)) {
            if (!isset($api)) {
                throw new InvalidArgumentException('Parameter api is required');
            }

            if (!isset($domain)) {
                throw new InvalidArgumentException('Parameter domain is required');
            }

            $mailgun = new \Fabric8\Mailer\Mailgun($api, $domain);
        }

        return $mailgun;
    }
}

if (!function_exists('mandrill')) {
    /**
     * @param string $api
     * @return Fabric8\Mailer\Mandrill
     */
    function mandrill($api = null)
    {
        static $mandrill;

        if (!isset($mandrill)) {
            if (!isset($api)) {
                throw new InvalidArgumentException('Parameter api is required');
            }

            $mandrill = new \Fabric8\Mailer\Mandrill($api);
        }

        return $mandrill;
    }
}
