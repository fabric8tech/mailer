<?php namespace Fabric8\Mailer;

use Swift_Message;

class Mandrill implements MailerContract
{
    private $api;

    private $mandrill;

    public function __construct($api)
    {
        $this->api = $api;

        $this->mandrill = new \Mandrill($api);
    }

    public function send(Swift_Message $message)
    {
        $data = $this->data($message);

        return $this->mandrill->messages->send($data);
    }

    protected function data(Swift_Message $message)
    {
        $data = [];

        if ($message->getContentType() === 'text/html') {
            $data['html'] = $message->getBody();
        } else {
            $data['text'] = $message->getBody();
        }

        $data['subject'] = $message->getSubject();

        $data['from_email'] = array_keys($message->getFrom())[0];
        $data['from_name'] = $message->getFrom()[$data['from_email']];

        $data['to'] = [];

        $data = $this->makeEmailDataForApi($data, $message->getTo(), 'to');
        $data = $this->makeEmailDataForApi($data, $message->getCc(), 'cc');
        $data = $this->makeEmailDataForApi($data, $message->getBcc(), 'bcc');

        if ($message->getReplyTo()) {
            $data['headers']['Reply-To'] = $this->makeEmailHeader($message->getReplyTo());
        }

        return $data;
    }

    protected function makeEmailDataForApi($data, $emails, $type = 'to')
    {
        if (!empty($emails) and is_array($emails)) {
            foreach ($emails as $email => $name) {
                $emailData = ['email' => $email, 'type' => $type];

                if (!empty($name)) {
                    $emailData['name'] = $name;
                }

                $data['to'][] = $emailData;
            }
        }

        return $data;
    }

    protected function makeEmailHeader($emails)
    {
        if (!is_array($emails)) {
            return null;
        }

        $headers = [];

        foreach ($emails as $email => $name) {
            if (empty($name)) {
                $headers[] = $email;
            } else {
                $headers[] = "'{$name}' <{$email}>";
            }
        }

        return implode(', ', $headers);
    }
}
