<?php namespace Fabric8\Mailer;

use Swift_Message;

interface MailerContract
{
    public function send(Swift_Message $message);
}
