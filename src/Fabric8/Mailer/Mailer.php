<?php namespace Fabric8\Mailer;

use Swift_Message;

class Mailer
{
    protected $transport;

    public function __construct(MailerContract $transport)
    {
        $this->transport = $transport;
    }

    public function send($to, $subject, $body, $headers = null)
    {
        $message = Swift_Message::newInstance($subject);

        $message = $this->addToFromString($to, $message);

        $message->setBody($body, 'text/html');

        if (is_string($headers)) {
            $message = $this->makeHeadersFromString($headers, $message);
        }

        return $this->transport->send($message);
    }

    public function setTransport(MailerContract $transport)
    {
        $this->transport = $transport;
    }

    /**
     * @param string $to
     * @param Swift_Message $message
     *
     * @return Swift_Message
     */
    protected function addToFromString($to, Swift_Message $message)
    {
        $tos = explode(',', $to);

        foreach ($tos as $to) {
            $message->addTo(trim($to));
        }

        return $message;
    }

    /**
     * @param string $headers
     * @param Swift_Message $message
     * @return Swift_Message
     */
    protected function makeHeadersFromString($headers, Swift_Message $message)
    {
        $headers = $this->parseRfc822Headers($headers);

        if (isset($headers['from'])) {
            $headers['from'] = $this->parseRfc822Emails($headers['from']);

            foreach ($headers['from'] as $email => $name) {
                $message->addFrom($email, $name);
            }
        }

        if (isset($headers['cc'])) {
            $headers['cc'] = $this->parseRfc822Emails($headers['cc']);

            foreach ($headers['cc'] as $email => $name) {
                $message->addCc($email, $name);
            }
        }

        if (isset($headers['bcc'])) {
            $headers['bcc'] = $this->parseRfc822Emails($headers['bcc']);

            foreach ($headers['bcc'] as $email => $name) {
                $message->addBcc($email, $name);
            }
        }

        if (isset($headers['reply-to'])) {
            $headers['reply-to'] = $this->parseRfc822Emails($headers['reply-to']);

            foreach ($headers['reply-to'] as $email => $name) {
                $message->addReplyTo($email, $name);
            }
        }

        return $message;
    }

    /**
     * @param string $headers
     *
     * @return stdClass
     */
    protected function parseRfc822Headers($headers)
    {
        $parsed = [];
        $blocks = preg_split('/\n\n/', $headers);
        $matches = [];
        foreach ($blocks as $i => $block) {
            $parsed[$i] = array();
            $lines = preg_split('/\n(([\w.-]+)\: *((.*\n\s+.+)+|(.*(?:\n))|(.*))?)/', $block, -1, PREG_SPLIT_DELIM_CAPTURE);
            foreach ($lines as $line) {
                if (preg_match('/^\n?([\w.-]+)\: *((.*\n\s+.+)+|(.*(?:\n))|(.*))?$/', $line, $matches)) {
                    $parsed[$i][strtolower($matches[1])] = preg_replace('/\n +/', ' ', trim($matches[2]));
                }
            }
        }

        return isset($parsed[0]) ? $parsed[0] : null;
    }

    /**
     * @param string $email
     *
     * @return array
     */
    protected function parseRfc822Emails($email)
    {
        $emails = [];

        if (preg_match_all('/\s*"?([^><,"]+)"?\s*((?:<[^><,]+>)?)\s*/', $email, $matches, PREG_SET_ORDER) > 0) {
            foreach ($matches as $m) {
                if (!empty($m[2])) {
                    $emails[trim($m[2], '<>')] = $m[1];
                } else {
                    $emails[$m[1]] = null;
                }
            }
        }

        return $emails;
    }
}
