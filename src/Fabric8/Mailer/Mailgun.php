<?php namespace Fabric8\Mailer;

use Swift_Message;

class Mailgun implements MailerContract
{
    private $api;

    private $domain;

    private $mailgun;

    public function __construct($api, $domain)
    {
        $this->api = $api;
        $this->domain = $domain;

        $this->mailgun = new \Mailgun\Mailgun($api);
    }

    public function send(Swift_Message $message)
    {
        $data = $this->data($message);

        return $this->mailgun->sendMessage($this->domain, $data);
    }

    protected function data(Swift_Message $message)
    {
        $data = [];

        if ($message->getContentType() === 'text/html') {
            $data['html'] = $message->getBody();
        } else {
            $data['text'] = $message->getBody();
        }

        $data['subject'] = $message->getSubject();

        if ($message->getFrom()) {
            $data['from'] = $this->makeEmailHeader($message->getFrom());
        }

        if ($message->getTo()) {
            $data['to'] = $this->makeEmailHeader($message->getTo());
        }

        if ($message->getCc()) {
            $data['cc'] = $this->makeEmailHeader($message->getCc());
        }

        if ($message->getBcc()) {
            $data['bcc'] = $this->makeEmailHeader($message->getBcc());
        }

        if ($message->getReplyTo()) {
            $data['h:Reply-To'] = $this->makeEmailHeader($message->getReplyTo());
        }

        return $data;
    }

    protected function makeEmailHeader($emails)
    {
        if (!is_array($emails)) {
            return null;
        }

        $headers = [];

        foreach ($emails as $email => $name) {
            if (empty($name)) {
                $headers[] = $email;
            } else {
                $headers[] = "'{$name}' <{$email}>";
            }
        }

        return implode(', ', $headers);
    }
}
